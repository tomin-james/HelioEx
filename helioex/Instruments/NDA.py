from helioex.ModelClass.InstrumentModel import Instrument_Meta, Instrument_Func
from datetime import datetime
import os

import numpy as np
import zipfile
from spacepy import pycdf
from helioex.utils import Utility
import astropy.units as u
from helioex.DataSources.Nancay import NancayObs
from helioex.DataSources.LocalFolder import LocalFolder

import matplotlib.dates as plt_dates
import matplotlib.ticker as mticker
from matplotlib import cm


class NDA_Meta(Instrument_Meta):
    def __init__(self, **kwargs):
        self._launch_date = datetime(1997, 1, 1, 0, 0, 0)
        self._space_based = False
        self._measurement_unit = [144, 1014]*u.hertz
        self._sensing_type = 'remote'
        self._end_of_life = False
        if self._end_of_life:
            tr = Utility.time_range_(self._launch_date, self._end_of_life)
        else:
            tr = Utility.time_range_(self._launch_date, datetime.now())
        self._data_range = tr
        self._quant_measured = 'Radio Flux'
        self._plot_type = 'spectrogram'

    @property
    def launch_date(self):
        return print(self._launch_date)

    @property
    def space_based(self):
        return self._space_based

    @property
    def sensing_type(self):
        return self._sensing_type

    @property
    def end_of_life(self):
        return print(self._end_of_life)

    @property
    def data_range(self):
        return self._data_range

    @property
    def summarise(self):
        return self._summarise


class NDA_Func(Instrument_Func):

    def fetch_data(self, tr, **kwargs):
        config = kwargs['config']

        self.tr = tr
        self.instrument = type(self).__name__
        self.NancayClient = NancayObs(tr=self.tr)
        self.LocalClient = LocalFolder(tr=self.tr, inst_name=self.instrument)

        nda_file_path = self.LocalClient.check_file()
        if len(nda_file_path) == 0:
            nda_browse_status, zip_file_path = self.NancayClient.check_file()
            if nda_browse_status != 404:
                with zipfile.ZipFile(zip_file_path, 'r') as zip_ref:
                    zip_ref.extractall(self.LocalClient.OUT_DIR)
                os.remove(zip_file_path)
                nda_file_path = self.LocalClient.check_file()
        if len(nda_file_path) == 1:
            nda_cdf = pycdf.CDF(nda_file_path[0])
            nda_freq = nda_cdf['Frequency'][...]
            nda_time = nda_cdf['Epoch'][...]
            nda_flux = nda_cdf['LL'][...]
        elif len(nda_file_path) == 0:
            nda_freq = np.arange(150, 3500)
            nda_time = Utility.generate_datearr(
                tr.start.to_datetime(), tr.end.to_datetime())
            nda_flux = np.zeros((nda_time.shape[0], nda_freq.shape[0]))
        nda_data = {}
        nda_data['frequency'] = nda_freq
        nda_data['time'] = nda_time
        nda_data['data'] = nda_flux

        return nda_data

    def plot_data(self, _data, axes):
        nda_idx = Utility.find_time_idx(self.tr, _data['time'])
        nda_flux = Utility.bck_sub_normalize(_data['data'][nda_idx, :])
        nda_time = _data['time'][nda_idx]
        nda_freq = _data['frequency']
        nda_extent = [plt_dates.date2num(nda_time[0]), plt_dates.date2num(nda_time[-1]),
                      nda_freq[-1], nda_freq[0]]
        axes.imshow(nda_flux.T, extent=nda_extent,
                    aspect='auto', cmap=cm.gist_heat_r)
        axes.xaxis_date()
        date_format = plt_dates.DateFormatter('%H:%M')
        axes.xaxis.set_major_formatter(date_format)
        axes.set_yscale('log')
        axes.set_ylabel('Frequency(MHZ)')
        axes.spines['bottom'].set_visible(False)
        axes.spines['top'].set_visible(False)
        axes.yaxis.set_minor_formatter(mticker.NullFormatter())
        axes.yaxis.set_ticks_position('both')
        axes.text(1.03, 0.6, 'NDA', horizontalalignment='center', rotation=90,
                  verticalalignment='center', transform=axes.transAxes)
