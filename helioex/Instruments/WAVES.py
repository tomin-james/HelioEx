from helioex.ModelClass.InstrumentModel import Instrument_Meta, Instrument_Func
from helioex.utils import Utility
from datetime import datetime
from helioex.DataSources.CDAWEb import CDAWEb
import astropy.units as u
import numpy as np
import matplotlib.dates as plt_dates
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable


class WAVES_Meta(Instrument_Meta):
    def __init__(self, **kwargs):
        self._launch_date = datetime(1997, 1, 1, 0, 0, 0)
        self._space_based = True
        self._measurement_unit = [144, 1014]*u.hertz
        self._sensing_type = 'remote'
        self._end_of_life = False
        if self._end_of_life:
            tr = Utility.time_range_(self._launch_date, self._end_of_life)
        else:
            tr = Utility.time_range_(self._launch_date, datetime.now())
        self._data_range = tr
        self._quant_measured = 'Radio Flux'
        self._plot_type = 'spectrogram'

    @property
    def launch_date(self):
        return print(self._launch_date)

    @property
    def space_based(self):
        return self._space_based

    @property
    def sensing_type(self):
        return self._sensing_type

    @property
    def end_of_life(self):
        return print(self._end_of_life)

    @property
    def data_range(self):
        return self._data_range

    @property
    def summarise(self):
        return self._summarise


class WAVES_Func(Instrument_Func):

    def fetch_data(self, tr, **kwargs):
        config = kwargs['config']

        self.tr = tr
        self.instrument = type(self).__name__
        self.CDAWebclient = CDAWEb(self.tr, self.instrument)
        data = self.CDAWebclient.check_file()

        waves_data = {}
        waves_data['time'] = data['Epoch']
        waves_data['rad2'] = data['E_VOLTAGE_RAD2']
        waves_data['rad1'] = data['E_VOLTAGE_RAD1']

        return waves_data

    def plot_data(self, _data, axes):
        waves_idx = Utility.find_time_idx(self.tr, np.asarray(_data['time']))
        waves_time = _data['time'][waves_idx]
        rad1_flux = Utility.bck_sub_normalize(
            np.asarray(_data['rad1'][waves_idx, :]))
        rad2_flux = Utility.bck_sub_normalize(
            np.asarray(_data['rad2'][waves_idx, :]))

        axes.imshow(rad2_flux.T, extent=[plt_dates.date2num(waves_time[0]),
                                         plt_dates.date2num(waves_time[-1]),
                                         13.825, 1.075], aspect='auto', cmap=cm.gist_heat_r)
        axes.set_yscale('log')
        axes.spines['top'].set_visible(False)
        axes.spines['bottom'].set_visible(False)
        axes.yaxis.set_ticks_position('both')

        divider = make_axes_locatable(axes)
        cax = divider.append_axes("top", size="100%", pad=0.005)
        cax.set_yscale('log')
        cax.imshow(rad1_flux.T, extent=[plt_dates.date2num(waves_time[0]),
                                        plt_dates.date2num(waves_time[-1]),
                                        1.04, 0.02], aspect='auto', cmap=cm.gist_heat_r)
        cax.spines['bottom'].set_visible(False)
        cax.yaxis.set_ticks_position('both')
        cax.set_xticks([])
        axes.text(1.03, 0.8, 'WIND/WAVES', horizontalalignment='center', rotation=90,
                  verticalalignment='center', transform=axes.transAxes)

        axes.xaxis_date()
        cax.xaxis_date()
        date_format = plt_dates.DateFormatter('%H:%M')
        axes.xaxis.set_major_formatter(date_format)
        cax.xaxis.set_major_formatter(date_format)
