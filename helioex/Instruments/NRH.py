from helioex.ModelClass.InstrumentModel import Instrument_Meta, Instrument_Func
from helioex.utils import Utility
from datetime import datetime, timedelta
from astropy.io import fits
from functools import partial

import astropy.units as u
import numpy as np
import matplotlib.dates as plt_dates
import matplotlib.ticker as mticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm
from matplotlib import cm

from helioex.DataSources.Mesopl import Mesopl
from helioex.DataSources.LocalFolder import LocalFolder


class NRH_Meta(Instrument_Meta):
    def __init__(self, **kwargs):
        self._launch_date = datetime(1997, 1, 1, 0, 0, 0)
        self._space_based = False
        self._measurement_unit = [150, 450]*u.hertz
        self._sensing_type = 'remote'
        self._end_of_life = False
        if self._end_of_life:
            tr = Utility.time_range_(self._launch_date, self._end_of_life)
        else:
            tr = Utility.time_range_(self._launch_date, datetime.now())
        self._data_range = tr
        self._quant_measured = 'Radio Flux'
        self._plot_type = 'spectrogram'

    @property
    def launch_date(self):
        return print(self._launch_date)

    @property
    def space_based(self):
        return self._space_based

    @property
    def sensing_type(self):
        return self._sensing_type

    @property
    def end_of_life(self):
        return print(self._end_of_life)

    @property
    def data_range(self):
        return self._data_range

    @property
    def summarise(self):
        return self._summarise


class NRH_Func(Instrument_Func):

    def fetch_data(self, tr, **kwargs):
        self.norm_flux = lambda flux_: (
            (flux_ - flux_.min())/(flux_.max()-flux_.min()))*255
        self.h60_files = lambda files: [
            file for file in files if 'h60' in file]
        self.h70_files = lambda files: [
            file for file in files if 'h70' in file]
        config = kwargs['config']

        self.tr = tr
        self.instrument = type(self).__name__
        self.MesoplClient = Mesopl(
            tr=self.tr, at_home=config['at_home'], inst_name=self.instrument)
        self.LocalClient = LocalFolder(tr=self.tr, inst_name=self.instrument)

        nrh1d_file = self.LocalClient.check_file()
        if not nrh1d_file:
            self.MesoplClient.check_file(self.LocalClient)
            nrh1d_file = self.LocalClient.check_file()
        nrh1d_file.sort()
        nrh_h60 = self.h60_files(nrh1d_file)
        if not nrh_h60:
            nrh1d_file = self.h70_files(nrh1d_file)
        else:
            nrh1d_file = nrh_h60
        if len(nrh1d_file) > 1:
            ew_flux, sn_flux, nrh_time = self.read_nrh(nrh1d_file[0])
            for ii in range(1, len(nrh1d_file)):
                ew_flux_, sn_flux_, nrh_time_ = self.read_nrh(nrh1d_file[ii])
                missed_nrh_time = np.arange(nrh_time[-1],
                                            nrh_time_[0],
                                            timedelta(seconds=10)).astype(datetime)
                nrh_time = np.concatenate([nrh_time, missed_nrh_time])
                ew_flux = np.vstack(
                    [ew_flux, np.zeros((len(missed_nrh_time), ew_flux.shape[1]))])
                sn_flux = np.vstack(
                    [sn_flux, np.zeros((len(missed_nrh_time), sn_flux.shape[1]))])
                ew_flux = np.vstack([ew_flux, ew_flux_])
                sn_flux = np.vstack([sn_flux, sn_flux_])
                nrh_time = np.concatenate([nrh_time, nrh_time_])
        else:
            ew_flux, sn_flux, nrh_time = self.read_nrh(nrh1d_file[0])
        nrh1d_flux = {}
        nrh1d_flux['time'] = nrh_time
        missed_nrh_time = np.arange(nrh1d_flux['time'][-1],
                                    tr.end.datetime+timedelta(seconds=100),
                                    timedelta(seconds=10)).astype(datetime)
        nrh1d_flux['ew'] = ew_flux
        nrh1d_flux['sn'] = sn_flux
        if len(missed_nrh_time) > 10:
            nrh1d_flux['time'] = np.concatenate(
                [nrh1d_flux['time'], missed_nrh_time])
            nrh1d_flux['ew'] = np.vstack([nrh1d_flux['ew'],
                                          np.zeros((len(missed_nrh_time), nrh1d_flux['ew'].shape[1]))])
            nrh1d_flux['sn'] = np.vstack([nrh1d_flux['sn'],
                                          np.zeros((len(missed_nrh_time), nrh1d_flux['sn'].shape[1]))])
        return nrh1d_flux

    def read_nrh(self, file):
        hdul = fits.open(file)
        ew_flux = hdul[1].data['STOKESI'].sum(axis=2)
        sn_flux = hdul[1].data['STOKESI'].sum(axis=1)
        ew_flux = Utility.bck_sub_normalize(ew_flux)
        ew_flux = self.norm_flux(ew_flux)
        sn_flux = Utility.bck_sub_normalize(sn_flux)
        sn_flux = self.norm_flux(sn_flux)
        map_func = partial(Utility.convert_seconds_datetime_nrh1d,
                           date_obs=hdul[1].header['DATE-OBS'])
        nrh_time = np.asarray(list(map(map_func, hdul[1].data['TIME']/1000)))
        return ew_flux, sn_flux, nrh_time

    def plot_data(self, _data, axes):
        nrh_idx = Utility.find_time_idx(self.tr, _data['time'])
        nrh_time = _data['time'][nrh_idx]
        nrh1d_sn = _data['sn'][nrh_idx, :]
        nrh1d_ew = _data['ew'][nrh_idx, :]
        nrh_extent = [plt_dates.date2num(
            nrh_time[0]), plt_dates.date2num(nrh_time[-1]), -2, 2]
        axes.imshow(nrh1d_sn.T, extent=nrh_extent, aspect='auto', origin='lower',
                    norm=LogNorm(vmin=nrh1d_sn.min()+0.02,
                                 vmax=nrh1d_sn.max()), cmap=cm.gist_heat_r)
        divider = make_axes_locatable(axes)
        cax = divider.append_axes("bottom", size="100%", pad=0.005)
        cax.imshow(nrh1d_ew.T, extent=nrh_extent, aspect='auto', origin='lower',
                   norm=LogNorm(vmin=nrh1d_ew.min()+0.02,
                                vmax=nrh1d_ew.max()), cmap=cm.gist_heat_r)
        # cax.xaxis_date()
        #date_format = plt_dates.DateFormatter('%H:%M')
        axes.set_xticklabels([])
        cax.set_xticklabels([])
        axes.set_ylabel('EW (Rs)')
        cax.set_ylabel('SN (Rs)')
        axes.text(1.03, -0.2, 'NRH 150 MHz', horizontalalignment='center', rotation=90,
                  verticalalignment='center', transform=axes.transAxes)
        axes.xaxis_date()
        date_format = plt_dates.DateFormatter('%H:%M')
        axes.xaxis.set_major_formatter(date_format)
