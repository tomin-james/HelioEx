from helioex.ModelClass.InstrumentModel import Instrument_Meta, Instrument_Func
from datetime import datetime
import numpy as np
from helioex.utils import Utility
import astropy.units as u
from my_sunpy.sunpy.net import Fido, attrs as a
from my_sunpy.sunpy.time import TimeRange, is_time_in_given_format, parse_time
from my_sunpy.sunpy.io import read_file
from collections import OrderedDict
from astropy.time import Time, TimeDelta
from my_sunpy.sunpy.util.metadata import MetaDict
import matplotlib.dates as plt_dates
import matplotlib.ticker as mticker


class GOES_Meta(Instrument_Meta):

    '''
    Meta class for GOES instrument
    '''

    def __init__(self):
        self._launch_date = datetime(1986, 1, 1, 0, 0, 0)
        self._space_based = True
        self._measurement_unit = u.W/u.m**2
        self._sensing_type = 'remote'
        self._end_of_life = False
        if self._end_of_life:
            tr = Utility.time_range_(self._launch_date, self._end_of_life)
        else:
            tr = Utility.time_range_(self._launch_date, datetime.now())
        self._data_range = tr
        self._quant_measured = 'Soft X-ray Irradiance'
        self._plot_type = 'lightcurve'

    @property
    def launch_date(self):
        return print(self._launch_date)

    @property
    def space_based(self):
        return self._space_based

    @property
    def sensing_type(self):
        return self._sensing_type

    @property
    def end_of_life(self):
        return print(self._end_of_life)

    @property
    def data_range(self):
        return self._data_range

    @property
    def summarise(self):
        return self._summarise


class GOES_Func(Instrument_Func):

    def fetch_data(self, tr, **kwargs):
        self.tr = tr
        results_goes = Fido.search(a.Time(self.tr), a.Instrument('XRS'))
        files_goes = Fido.fetch(results_goes)
        return self._parse_file(files_goes[0])

    def _get_goes_sat_num(self, start, end):
        """
        Parses the query time to determine which GOES satellite to use.
        Parameters
        ----------
        filepath : `str`
            The path to the file you want to parse.
        """
        goes_operational = {
            2: TimeRange('1980-01-04', '1983-05-01'),
            5: TimeRange('1983-05-02', '1984-08-01'),
            6: TimeRange('1983-06-01', '1994-08-19'),
            7: TimeRange('1994-01-01', '1996-08-14'),
            8: TimeRange('1996-03-21', '2003-06-19'),
            9: TimeRange('1997-01-01', '1998-09-09'),
            10: TimeRange('1998-07-10', '2009-12-02'),
            11: TimeRange('2006-06-20', '2008-02-16'),
            12: TimeRange('2002-12-13', '2007-05-09'),
            13: TimeRange('2006-08-01', '2006-08-01'),
            14: TimeRange('2009-12-02', '2010-11-05'),
            15: TimeRange('2010-09-01', Time.now()),
        }

        sat_list = []
        for sat_num in goes_operational:
            if (goes_operational[sat_num].start <= start <= goes_operational[sat_num].end and
                    goes_operational[sat_num].start <= end <= goes_operational[sat_num].end):
                # if true then the satellite with sat_num is available
                sat_list.append(sat_num)

        if not sat_list:
            # if no satellites were found then raise an exception
            raise Exception('No operational GOES satellites within time range')
        else:
            return sat_list

    def _parse_file(self, filepath):
        """
        Parses a GOES/XRS FITS file.
        Parameters
        ----------
        filepath : `str`
            The path to the file you want to parse.
        """
        hdus = read_file(filepath)
        return self._parse_hdus(hdus)

    def _parse_hdus(self, hdulist):
        """
        Parses a GOES/XRS FITS `~astropy.io.fits.HDUList` from a FITS file.
        Parameters
        ----------
        hdulist : `astropy.io.fits.HDUList`
            A HDU list.
        """
        header = MetaDict(OrderedDict(hdulist[0].header))
        if len(hdulist) == 4:
            if is_time_in_given_format(hdulist[0].header['DATE-OBS'], '%d/%m/%Y'):
                start_time = Time.strptime(
                    hdulist[0].header['DATE-OBS'], '%d/%m/%Y')
            elif is_time_in_given_format(hdulist[0].header['DATE-OBS'], '%d/%m/%y'):
                start_time = Time.strptime(
                    hdulist[0].header['DATE-OBS'], '%d/%m/%y')
            else:
                raise ValueError("Date not recognized")
            xrsb = hdulist[2].data['FLUX'][0][:, 0]
            xrsa = hdulist[2].data['FLUX'][0][:, 1]
            seconds_from_start = hdulist[2].data['TIME'][0]
        elif 1 <= len(hdulist) <= 3:
            start_time = parse_time(header['TIMEZERO'], format='utime')
            seconds_from_start = hdulist[0].data[0]
            xrsb = hdulist[0].data[1]
            xrsa = hdulist[0].data[2]
        else:
            raise ValueError("Don't know how to parse this file")

        times = start_time + TimeDelta(seconds_from_start*u.second)
        times.precision = 9
        times = np.asarray([datetime.strptime(
            str(ii)[:-3], '%Y-%m-%dT%H:%M:%S.%f') for ii in times.isot.astype('datetime64')])

        # remove bad values as defined in header comments
        xrsb[xrsb == -99999] = np.NaN
        xrsa[xrsa == -99999] = np.NaN

        # fix byte ordering
        newxrsa = xrsa.byteswap().newbyteorder()
        newxrsb = xrsb.byteswap().newbyteorder()

        goes_data = {
            'time': times,
            'data': np.vstack((newxrsa, newxrsb)).T,
            'labels': ('xrsa', 'xrsb')
        }

        # Add the units
        units = OrderedDict([('xrsa', u.W/u.m**2),
                             ('xrsb', u.W/u.m**2)])
        return goes_data

    def plot_data(self, _data, axes, align_time=False):

        goes_idx = Utility.find_time_idx(self.tr, _data['time'])
        goes_time = _data['time'][goes_idx]
        goes_flux = _data['data'][goes_idx, :]
        goes_labels = _data['labels']

        goes_dates = plt_dates.date2num(parse_time(goes_time).datetime)
        l1, = axes.plot_date(
            goes_time, goes_flux[:, 0], '-', color='blue', lw=2)
        l2, = axes.plot_date(
            goes_time, goes_flux[:, 1], '-', color='red', lw=2)
        axes.legend((l1, l2), goes_labels, loc='upper right')
        axes.set_yscale("log")
        axes.set_ylim(1e-9, 1e-2)
        axes.set_ylabel('Watts m$^{-2}$')

        ax2 = axes.twinx()
        ax2.set_yscale("log")
        ax2.set_ylim(1e-9, 1e-2)
        labels = ["A", "B", "C", "M", "X"]
        centers = np.logspace(-7.5, -3.5, len(labels))
        ax2.yaxis.set_minor_locator(mticker.FixedLocator(centers))
        ax2.set_yticklabels(labels, minor=True)
        ax2.set_yticklabels([])

        axes.yaxis.grid(True, 'major')
        axes.xaxis.grid(False, 'major')
        # if asked for aligning time then do it at the time of final plot output
        if not align_time:
            date_format = plt_dates.DateFormatter('%H:%M')
            axes.xaxis.set_major_formatter(date_format)
