from helioex.ModelClass.InstrumentModel import Instrument_Meta, Instrument_Func
from helioex.utils import Utility
from datetime import datetime
from helioex.DataSources.Mesopl import Mesopl
from helioex.DataSources.LocalFolder import LocalFolder
from functools import partial
from astropy.io import fits
import astropy.units as u
import numpy as np
import matplotlib.dates as plt_dates
from matplotlib.colors import LogNorm
import matplotlib.ticker as mticker
from matplotlib import cm


class ORFEE_Meta(Instrument_Meta):
    def __init__(self, **kwargs):
        self._launch_date = datetime(1997, 1, 1, 0, 0, 0)
        self._space_based = False
        self._measurement_unit = [144, 1014]*u.hertz
        self._sensing_type = 'remote'
        self._end_of_life = False
        if self._end_of_life:
            tr = Utility.time_range_(self._launch_date, self._end_of_life)
        else:
            tr = Utility.time_range_(self._launch_date, datetime.now())
        self._data_range = tr
        self._quant_measured = 'Radio Flux'
        self._plot_type = 'spectrogram'

    @property
    def launch_date(self):
        return print(self._launch_date)

    @property
    def space_based(self):
        return self._space_based

    @property
    def sensing_type(self):
        return self._sensing_type

    @property
    def end_of_life(self):
        return print(self._end_of_life)

    @property
    def data_range(self):
        return self._data_range

    @property
    def summarise(self):
        return self._summarise


class ORFEE_Func(Instrument_Func):

    def fetch_data(self, tr, **kwargs):
        config = kwargs['config']

        self.tr = tr
        self.instrument = type(self).__name__
        self.MesoplClient = Mesopl(
            tr=self.tr, at_home=config['at_home'], inst_name=self.instrument)
        self.LocalClient = LocalFolder(tr=self.tr, inst_name=self.instrument)

        orfee_files = self.LocalClient.check_file()
        if len(orfee_files) == 0:
            self.MesoplClient.check_file(self.LocalClient)
            orfee_files = self.LocalClient.check_file()
        orfee_files.sort()
        (Data_I, time_orfees, freq_orfees, date_obs,
         *na) = self.read_orfee(orfee_files[0])
        grt_18h = np.where((time_orfees/1000) > 65000)[0]
        if len(grt_18h) > 1:
            time_orfees = time_orfees[:grt_18h[0]]
            Data_I = Data_I[:grt_18h[0], :]
        map_func = partial(Utility.convert_seconds_datetime, date_obs=date_obs)
        orfee_time = np.asarray(list(map(map_func, time_orfees/1000)))
        for ii in range(1, len(orfee_files)):
            (Data_I_, time_orfees_, freq_orfees, date_obs,
             *na) = self.read_orfee(orfee_files[ii])
            grt_18h = np.where((time_orfees_/1000) > 65000)[0]
            if len(grt_18h) > 1:
                time_orfees_ = time_orfees_[:grt_18h[0]]
                Data_I_ = Data_I_[:grt_18h[0], :]
            orfee_time_ = np.asarray(list(map(map_func, time_orfees_/1000)))
            Data_I = np.vstack([Data_I, Data_I_])
            orfee_time = np.concatenate([orfee_time, orfee_time_])

        decimetric_radio = {}
        decimetric_radio['time'] = orfee_time
        decimetric_radio['data'] = Data_I
        decimetric_radio['freq'] = freq_orfees
        return decimetric_radio

    def read_orfee(self, file):
        hdu = fits.open(file)
        header_file = hdu[0].header  # Header file
        header_frequency = hdu[1].header  # Header frequency
        header_data = hdu[2].header  # Header data
        frequency = hdu[1].data  # data Extend1 (ferquency)
        Data = hdu[2].data  # data Extend2 (data)
        date_obs = header_file[4]  # Date observation start
        time_start_obs = header_file[5]  # Time observation start
        time_end_obs = header_file[7]  # Date observation End
        time_integration = header_data[9]  # Time integration
        data_SI_B1 = Data.STOKESI_B1  # Data table STOKES I of band 1
        data_SV_B1 = Data.STOKESV_B1  # Data table STOKES V of band 1
        data_SI_B2 = Data.STOKESI_B2  # Data table STOKES I of band 2
        data_SV_B2 = Data.STOKESV_B2  # Data table STOKES V of band 2
        data_SI_B3 = Data.STOKESI_B3  # Data table STOKES I of band 3
        data_SV_B3 = Data.STOKESV_B3  # Data table STOKES V of band 3
        data_SI_B4 = Data.STOKESI_B4  # Data table STOKES I of band 4
        data_SV_B4 = Data.STOKESV_B4  # Data table STOKES V of band 4
        data_SI_B5 = Data.STOKESI_B5  # Data table STOKES I of band 5
        data_SV_B5 = Data.STOKESV_B5  # Data table STOKES V of ba444nd 5
        TIME_B1 = Data.TIME_B1  # Time table of band 1
        TIME_B2 = Data.TIME_B2  # Time table of band 2
        TIME_B3 = Data.TIME_B3  # Time table of band 3
        TIME_B4 = Data.TIME_B4  # Time table of band 4
        TIME_B5 = Data.TIME_B5  # Time table of band 5
        nbr_freq_b1 = frequency.NP_B1[0]  # Number of frenquencies of band 1
        nbr_freq_b2 = frequency.NP_B2[0]  # Number of frenquencies of band 2
        nbr_freq_b3 = frequency.NP_B3[0]  # Number of frenquencies of band 3
        nbr_freq_b4 = frequency.NP_B4[0]  # Number of frenquencies of band 4
        nbr_freq_b5 = frequency.NP_B5[0]  # Number of frenquencies of band 5
        freq_b1 = frequency.FREQ_B1[0]  # frenquencies table of band 1
        freq_b2 = frequency.FREQ_B2[0]  # frenquencies table of band 2
        freq_b3 = frequency.FREQ_B3[0]  # frenquencies table of band 3
        freq_b4 = frequency.FREQ_B4[0]  # frenquencies table of band 4
        freq_b5 = frequency.FREQ_B5[0]  # frenquencies table of band 5
        # creation_one_data_frequency_and_time_tables
        # frenquencies table
        Freq = np.concatenate((freq_b1, freq_b2, freq_b3, freq_b4, freq_b5))
        # Time = np.concatenate((TIME_B1,TIME_B2,TIME_B3,TIME_B4,TIME_B5)) #Time table
        # Time = TIME_B1 #Time table
        Data_I = np.concatenate(
            (data_SI_B1, data_SI_B2, data_SI_B3, data_SI_B4, data_SI_B5), axis=1)  # Data table
        # return data_SI_B1,data_SI_B2,data_SI_B3,data_SI_B4,data_SI_B5,TIME_B1,TIME_B2,TIME_B3,TIME_B4,TIME_B5,freq_b1,freq_b2,freq_b3,freq_b4,freq_b5,date_obs,time_start_obs,time_end_obs,time_integration
        return Data_I, TIME_B1, Freq, date_obs, time_start_obs, time_end_obs, time_integration

    def plot_data(self, _data, axes):
        orf_idx = Utility.find_time_idx(self.tr, _data['time'])
        orfees_flux = Utility.bck_sub_normalize(_data['data'][orf_idx, :])
        orfees_freq = _data['freq']
        orfees_time = _data['time'][orf_idx]
        orfees_extent = [plt_dates.date2num(orfees_time[0]), plt_dates.date2num(orfees_time[-1]),
                         orfees_freq[-1], orfees_freq[0]]
        axes.imshow(orfees_flux.T, aspect='auto',
                    extent=orfees_extent,
                    origin='upper', norm=LogNorm(vmin=orfees_flux.min()+0.05,
                                                 vmax=orfees_flux.max()), cmap=cm.gist_heat_r)
        # axes[ra_gr,].set_yscale('log')
        axes.set_yticks([1011])
        axes.set_yticklabels(['1000'])
        axes.set_yticks([140, 390, 537, 657, 773, 839, 896, 960], minor=True)
        axes.text(1.03, 0.2, 'ORFEE', horizontalalignment='center', rotation=90,
                  verticalalignment='center', transform=axes.transAxes)
        axes.xaxis_date()
        date_format = plt_dates.DateFormatter('%H:%M')
        axes.xaxis.set_major_formatter(date_format)
        axes.spines['bottom'].set_visible(False)
        axes.spines['top'].set_visible(False)
        axes.yaxis.set_minor_formatter(mticker.NullFormatter())
        axes.yaxis.set_ticks_position('both')
