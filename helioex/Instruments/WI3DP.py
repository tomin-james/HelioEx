from helioex.ModelClass.InstrumentModel import Instrument_Meta, Instrument_Func
from helioex.utils import Utility
from datetime import datetime
from helioex.DataSources.CDAWEb import CDAWEb
import numpy as np
import matplotlib.dates as plt_dates
import matplotlib.ticker as mticker


class WI3DP_Meta(Instrument_Meta):
    def __init__(self, **kwargs):
        self._launch_date = datetime(1997, 1, 1, 0, 0, 0)
        self._space_based = True
        self._measurement_unit = 'electrons / cm^2'
        self._sensing_type = 'in-situ'
        self._end_of_life = False
        if self._end_of_life:
            tr = Utility.time_range_(self._launch_date, self._end_of_life)
        else:
            tr = Utility.time_range_(self._launch_date, datetime.now())
        self._data_range = tr
        self._quant_measured = 'Particle Flux'
        self._plot_type = 'lightcurve'

    @property
    def launch_date(self):
        return print(self._launch_date)

    @property
    def space_based(self):
        return self._space_based

    @property
    def sensing_type(self):
        return self._sensing_type

    @property
    def end_of_life(self):
        return print(self._end_of_life)

    @property
    def data_range(self):
        return self._data_range

    @property
    def summarise(self):
        return self._summarise


class WI3DP_Func(Instrument_Func):

    def fetch_data(self, tr, **kwargs):
        config = kwargs['config']

        self.tr = tr
        self.instrument = type(self).__name__
        self.CDAWebclient = CDAWEb(self.tr, self.instrument)
        data = self.CDAWebclient.check_file()

        wi3dp_data = {}
        wi3dp_data['time'] = data['Epoch']
        wi3dp_data['data'] = data['FLUX_STACKED']
        wi3dp_data['labels'] = ['27 keV', '40 keV', '66 keV',
                                '108 keV', '181 keV', '310 keV', '515 keV']

        return wi3dp_data

    def plot_data(self, _data, axes):
        wi3dp_idx = Utility.find_time_idx(self.tr, np.asarray(_data['time']))
        wi3dp_time = _data['time'][wi3dp_idx]
        wi3dp_flux = np.asarray(_data['data'][wi3dp_idx, :])
        wi3dp_labels = _data['labels'][:4]

        # axis object
        plt_dates_3dp = plt_dates.date2num(wi3dp_time)

        l1, = axes.plot_date(plt_dates_3dp, wi3dp_flux[:, 0].ravel(), '-')
        l2, = axes.plot_date(plt_dates_3dp, wi3dp_flux[:, 1].ravel(), '-')
        l3, = axes.plot_date(plt_dates_3dp, wi3dp_flux[:, 2].ravel(), '-')
        l4, = axes.plot_date(plt_dates_3dp, wi3dp_flux[:, 3].ravel(), '-')
        axes.legend((l1, l2, l3, l4), wi3dp_labels, loc='upper right')

        axes.minorticks_on()
        axes.yaxis.set_minor_locator(mticker.MultipleLocator(5))
        axes.set_yscale("log")
        axes.set_ylabel('electrons/cm^2/ster/s')
        axes.yaxis.grid(True, 'major')
        axes.text(1.03, 0.2, 'WIND/3DP', horizontalalignment='center', rotation=90,
                  verticalalignment='center', transform=axes.transAxes)
        axes.xaxis_date()
        date_format = plt_dates.DateFormatter('%H:%M')
        axes.xaxis.set_major_formatter(date_format)
