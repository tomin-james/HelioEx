from helioex.ModelClass.InstrumentModel import Instrument_Meta, Instrument_Func
from datetime import datetime
import numpy as np
import astropy.units as u
from helioex.utils import Utility
import wget


from gbm import time as gbm_time
from gbm.binning.binned import rebin_by_time
from gbm.data import Ctime, Cspec

import matplotlib.dates as plt_dates
import matplotlib.ticker as mticker


class FERMI_Meta(Instrument_Meta):
    def __init__(self):
        self._launch_date = datetime(2009, 1, 1, 0, 0, 0)
        self._space_based = True
        self._measurment_unit = [0, 100000]*u.eV
        self._sensing_type = 'remote'
        self._end_of_life = False
        if self._end_of_life:
            tr = Utility.time_range_(self._launch_date, self._end_of_life)
        else:
            tr = Utility.time_range_(self._launch_date, datetime.now())
        self._data_range = tr
        self._quant_measured = 'HXR counts'
        self._plot_type = 'lightcurve'

    @property
    def launch_date(self):
        return print(self._launch_date)

    @property
    def space_based(self):
        return self._space_based

    @property
    def sensing_type(self):
        return self._sensing_type

    @property
    def end_of_life(self):
        return print(self._end_of_life)

    @property
    def data_range(self):
        return self._data_range

    @property
    def summarise(self):
        return self._summarise


class FERMI_Func(Instrument_Func):

    def fetch_data(self, tr, **kwargs):
        self.tr = tr
        det_no = 5
        yyyy = tr.start.datetime.year
        mm = tr.start.datetime.month
        dd = tr.start.datetime.day
        output_directory = './data'
        for ver_no in range(5):
            fermi_url = 'https://heasarc.gsfc.nasa.gov/FTP/fermi/data/gbm/daily/'+str(yyyy) + \
                '/'+str(mm).zfill(2) + \
                '/'+str(dd).zfill(2) + \
                '/current/glg_cspec_n'+str(det_no)+'_'+tr.start.strftime("%y%m%d")+'_v' + \
                str(ver_no).zfill(2)+'.pha'
            try:
                filename = wget.download(fermi_url, out=output_directory)
                break
            except:
                continue

        cspec = Cspec.open(filename)
        rebinned_ctime = cspec.rebin_time(rebin_by_time, 4.096)
        gbm_time_arr = np.asarray(list(map(gbm_time.Met.iso,
                                           map(gbm_time.Met, rebinned_ctime.data.time_centroids))))
        gbm_time_arr = np.asarray(
            [datetime.strptime(ii, '%Y-%m-%dT%H:%M:%S') for ii in gbm_time_arr])

        fermi_data = {
            'time': gbm_time_arr,
            'data': np.asarray((rebinned_ctime.data.counts[:, 0:5].sum(axis=1),
                                rebinned_ctime.data.counts[:, 5:12].sum(
                                    axis=1),
                                rebinned_ctime.data.counts[:, 12:22].sum(
                                    axis=1),
                                rebinned_ctime.data.counts[:, 22:34].sum(axis=1))).T,
            'labels': ('4-8 keV', '9-14 keV', '15-25 keV', '25-50 keV')
        }
        return fermi_data

    def plot_data(self, _data, axes):

        hxr_idx = Utility.find_time_idx(self.tr, _data['time'])
        hxr_time = _data['time'][hxr_idx]
        fermi_counts = _data['data'][hxr_idx, :]
        fermi_labels = _data['labels']
        # axis object
        hxr_dates = plt_dates.date2num(hxr_time)

        l1, = axes.plot_date(hxr_dates, fermi_counts[:, 0].ravel(), '-')
        l2, = axes.plot_date(hxr_dates, fermi_counts[:, 1].ravel(), '-')
        l3, = axes.plot_date(hxr_dates, fermi_counts[:, 2].ravel(), '-')
        l4, = axes.plot_date(hxr_dates, fermi_counts[:, 3].ravel(), '-')
        axes.legend((l1, l2, l3, l4), fermi_labels, loc='upper right')

        axes.minorticks_on()
        axes.yaxis.set_minor_locator(mticker.MultipleLocator(5))
        axes.set_yscale("log")
        axes.set_ylabel('counts/s')
        axes.yaxis.grid(True, 'major')
        axes.text(1.03, 0.2, 'FERMI', horizontalalignment='center', rotation=90,
                  verticalalignment='center', transform=axes.transAxes)
        axes.xaxis_date()
        date_format = plt_dates.DateFormatter('%H:%M')
        axes.xaxis.set_major_formatter(date_format)
