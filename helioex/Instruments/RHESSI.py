from helioex.ModelClass.InstrumentModel import Instrument_Meta, Instrument_Func
from my_sunpy.sunpy.net.dataretriever import RHESSIClient
from my_sunpy.sunpy.net import Fido, attrs as a
from astropy.io import fits
from my_sunpy.sunpy.time import parse_time
import numpy as np
from astropy.time import TimeDelta
import astropy.units as u


from helioex.utils import Utility
from datetime import datetime

import matplotlib.dates as plt_dates
import matplotlib.ticker as mticker


class RHESSI_Meta(Instrument_Meta):

    def __init__(self):
        self._launch_date = datetime(2002, 2, 5, 0, 0, 0)
        self._space_based = True
        #self._measurement_unit = [0,100000]*u.eV
        self._sensing_type = 'remote'
        self._end_of_life = datetime(2002, 2, 5, 0, 0, 0)
        if self._end_of_life:
            tr = Utility.time_range_(self._launch_date, self._end_of_life)
        else:
            tr = Utility.time_range_(self._launch_date, datetime.now())
        self._data_range = tr
        self._quant_measured = 'HXR counts'
        self._plot_type = 'lightcurve'

    @property
    def launch_date(self):
        return print(self._launch_date)

    @property
    def space_based(self):
        return self._space_based

    @property
    def sensing_type(self):
        return self._sensing_type

    @property
    def end_of_life(self):
        return print(self._end_of_life)

    @property
    def data_range(self):
        return self._data_range

    @property
    def summarise(self):
        return self._summarise


class RHESSI_Func(Instrument_Func):

    def fetch_data(self, tr, **kwargs):
        '''
        Fetch data from RHESSI servers established 
        in the DataSource method class.
        For RHESSI we rely on the FIDO class from SunPy.

        Parameters
        ----------
        timerange: 'timerange object'
              Must be a Sunpy TimeRange Object

        Returns
        -------
        rhessidata: `dict`
              A dictionary containing RHESSI data
        '''
        self.tr = tr
        rhessi_fido = RHESSIClient()

        results_rhessi = rhessi_fido.search(a.Time(tr))
        files_rhessi = rhessi_fido.fetch(results_rhessi)

        rhessi_header, rhessi_data = self._parse_observing_summary_hdulist(
            files_rhessi[0])
        return rhessi_data

    def _parse_observing_summary_hdulist(self, fits_filename):
        """
        Parse a RHESSI observation summary file.

        Parameters
        ----------
        hdulist : `list`
            The HDU list from the fits file.

        Returns
        -------
        out : `dict`
            Returns a dictionary.
        """
        hdulist = fits.open(fits_filename)
        header = hdulist[0].header

        reference_time_ut = parse_time(hdulist[5].data.field('UT_REF')[0],
                                       format='utime')
        time_interval_sec = hdulist[5].data.field('TIME_INTV')[0]
        # label_unit = fits[5].data.field('DIM1_UNIT')[0]
        # labels = fits[5].data.field('DIM1_IDS')
        labels = ('3 - 6 keV', '6 - 12 keV', '12 - 25 keV', '25 - 50 keV',
                  '50 - 100 keV', '100 - 300 keV', '300 - 800 keV',
                  '800 - 7000 keV', '7000 - 20000 keV')

        # The data stored in the fits file are "compressed" countrates stored as
        # one byte
        compressed_countrate = np.array(hdulist[6].data.field('countrate'))

        countrate = self._uncompress_countrate(compressed_countrate)
        dim = np.array(countrate[:, 0]).size
        time_array = parse_time(reference_time_ut) + \
            TimeDelta(time_interval_sec * np.arange(dim) * u.second)
        flags = hdulist[15].data['FLAGS']
        #  TODO generate the labels for the dict automatically from labels
        data = {'time': time_array.datetime, 'data': countrate,
                'labels': labels, 'flags': flags}
        hdulist.close()
        return header, data

    def _uncompress_countrate(self, compressed_countrate):
        """
        Convert the compressed count rate inside of observing summary file from a
        compressed byte to a true count rate.

        Parameters
        ----------
        compressed_countrate : `byte` array
            A compressed count rate returned from an observing summary file.

        References
        ----------
        `Hsi_obs_summ_decompress.pro 
        <https://hesperia.gsfc.nasa.gov/ssw/hessi/idl/qlook_archive/hsi_obs_summ_decompress.pro>`_
        """

        # Ensure uncompressed counts are between 0 and 255
        if (compressed_countrate.min() < 0) or (compressed_countrate.max() > 255):
            raise ValueError(
                f'Exepected uncompressed counts {compressed_countrate} to in range 0-255')

        # TODO Must be a better way than creating entire lookup table on each call
        ll = np.arange(0, 16, 1)
        lkup = np.zeros(256, dtype='int')
        _sum = 0
        for i in range(0, 16):
            lkup[16 * i:16 * (i + 1)] = ll * 2 ** i + _sum
            if i < 15:
                _sum = lkup[16 * (i + 1) - 1] + 2 ** i

        return lkup[compressed_countrate]

    def plot_data(self, _data, axes):
        hxr_idx = Utility.find_time_idx(self.tr, _data['time'])
        hxr_time = _data['time'][hxr_idx]
        rhessi_counts = _data['data'][hxr_idx, :]
        rhessi_labels = _data['labels'][:4]
        # axis object
        hxr_dates = plt_dates.date2num(hxr_time)

        # your code here  (chan 1)
        l1, = axes.plot_date(hxr_dates, rhessi_counts[:, 0].ravel(), '-')
        # your code here  (chan 2)
        l2, = axes.plot_date(hxr_dates, rhessi_counts[:, 1].ravel(), '-')
        # your code here  (chan 3)
        l3, = axes.plot_date(hxr_dates, rhessi_counts[:, 2].ravel(), '-')
        # your code here  (chan 4)
        l4, = axes.plot_date(hxr_dates, rhessi_counts[:, 3].ravel(), '-')
        axes.legend((l1, l2, l3, l4), rhessi_labels,
                    loc='upper right')

        axes.minorticks_on()
        axes.yaxis.set_minor_locator(mticker.MultipleLocator(5))
        axes.set_yscale("log")
        axes.set_ylabel('counts/s')
        axes.yaxis.grid(True, 'major')
        axes.text(1.03, 0.2, 'RHESSI', horizontalalignment='center', rotation=90,
                  verticalalignment='center', transform=axes.transAxes)
        axes.xaxis_date()
        date_format = plt_dates.DateFormatter('%H:%M')
        axes.xaxis.set_major_formatter(date_format)
