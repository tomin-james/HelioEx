from datetime import datetime
from datetime import timedelta
import numpy as np
from my_sunpy.sunpy.time import TimeRange


class Utility:
    '''
    A collection of functions to make life easier.
    '''

    @staticmethod
    def time_range_(dt1, dt2):
        '''
        Function to create a timerange object
        given two datetime instances.
        '''

        tr = TimeRange(dt1, dt2)
        return tr

    @staticmethod
    def bck_sub_normalize(flux):
        '''
        Function to normalize the spectrogram imaging.
        '''

        start_mean = flux.mean()
        while True:
            temp_Data_I = flux - np.mean(flux, axis=0)
            flux = temp_Data_I.clip(min=0)
            if (((start_mean - flux.mean())/start_mean) >= 0.5) & (flux.mean() > 0.1):
                start_mean = flux.mean()
            else:
                break
        flux = ((flux - flux.min())/(flux.max()-flux.min()))*255
        return flux

    @staticmethod
    def convert_seconds_datetime(sod_i, date_obs):
        '''
        A helper function to convert seconds of day to a datetime object
        '''
        return datetime.strptime(date_obs+'T'+str(timedelta(seconds=sod_i))[:-3], '%Y-%m-%dT%H:%M:%S.%f')

    @staticmethod
    def convert_seconds_datetime_nrh1d(sod_i, date_obs):
        '''
        A helper function to convert NRH time instances into datetime
        '''

        for fmt in ('%Y-%m-%dT%H:%M:%S.%f', '%Y-%m-%dT%H:%M:%S'):
            try:
                return datetime.strptime(date_obs+'T'+str(timedelta(seconds=round(sod_i+0.001, 5))), fmt)
            except ValueError:
                pass

    @staticmethod
    def convert_seconds_datetime_waves(sod_i, date_obs):
        '''
        A helper function to convert WAVES time instances into datetime
        '''
        return datetime.strptime(date_obs+'T'+str(timedelta(seconds=sod_i)), '%Y-%m-%dT%H:%M:%S.%f')

    @staticmethod
    def find_time_idx(tr, time_arr):
        '''
        Function to find the time enclosed between two given datetime points in a 
        datetime array
        '''
        time_idx = np.where((time_arr >= tr.start) &  # enter your code after removing the string
                            (time_arr <= tr.end))[0]
        return time_idx

    @staticmethod
    def create_tr_date_string(tr):
        '''
        Function to create datetime string
        '''
        return [str(tr.start.datetime.year),
                str(tr.start.datetime.month),
                str(tr.start.datetime.day)]

    @staticmethod
    def smooth(y, _pts):
        '''
        Function to do window smoothing
        '''
        box = np.ones(_pts)/_pts
        y_smooth = np.convolve(y, box, mode='same')
        return y_smooth

    @staticmethod
    def lightcurve_normalise(arr):
        '''
        Function to normalise one-dimensional timeseries data
        '''
        norm_data = (arr - arr.min())/(arr.max()-arr.min())
        return norm_data/norm_data.mean()

    @staticmethod
    def convert_date_time_string(date, time, end=0):
        date_string = date+' '+time
        if '.' not in date_string:
            date_string = date_string + '.0'
        if end == 1:
            return np.datetime64(datetime.strptime(date_string, '%Y/%m/%d %H:%M:%S.%f'))+np.timedelta64(500, 'ms')
        return np.datetime64(datetime.strptime(date_string, '%Y/%m/%d %H:%M:%S.%f'))

    @staticmethod
    def generate_datearr(time_start, time_end):

        datetime_arr = np.arange(time_start, time_end,
                                 np.timedelta64(1000, 'ms'))
        return datetime_arr
