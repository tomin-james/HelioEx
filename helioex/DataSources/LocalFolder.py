import glob
from helioex.utils import Utility


class LocalFolder:

    def __init__(self, tr, inst_name, out_dir=None):
        self.PATHS = ['/home/tjames/work_stuff/nrh_project/RADIO+RHESSI/data',
                      '/home/tjames/work_stuff/nrh_project']
        if out_dir is None:
            self.OUT_DIR = '/home/tjames/work_stuff/nrh_project/Radio+RHESSI/data/'
        else:
            self.OUT_DIR = out_dir
        self.instrument = inst_name
        self.tr = tr

    def check_status(self):
        return NotImplemented

    def is_datasource_for(self):
        return NotImplemented

    def get_file_name(self, tr):
        yyyy, mm, dd = Utility.create_tr_date_string(tr)
        inst_dict = {'NRH': 'nrh2_1*_h*'+yyyy+mm.zfill(2)+dd.zfill(2)+'*.fts',
                     'ORFEE': 'int_orf'+str(yyyy)+str(mm).zfill(2)+str(dd).zfill(2)+'*.fts',
                     'NDA': '*'+str(yyyy)+str(mm).zfill(2)+str(dd).zfill(2)+'*.cdf'}
        return inst_dict[self.instrument]

    def check_file(self):
        return glob.glob(self.OUT_DIR+self.get_file_name(self.tr))
