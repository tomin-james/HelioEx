from helioex.ModelClass.DataSourceModel import DataSource
from datetime import datetime
from helioex.utils import Utility
import pysftp
import re


class Mesopl(DataSource):

    def __init__(self, tr, at_home, inst_name):
        self.myHostname = "mesopl.obspm.fr"
        self.myUsername = "tjames"
        self.myPassword = "@JesusAppa1"
        self.thePort = 22
        self.mesopl_status = None
        self.filename_exp = None

        if at_home:
            self.myHostname = '127.0.0.1'
            self.thePort = 2222

        self.cnopts = pysftp.CnOpts()
        self.hostkeys = None

        if self.cnopts.hostkeys.lookup(self.myHostname) == None:
            print("New host - will accept any host key")
            # Backup loaded .ssh/known_hosts file
            hostkeys = self.cnopts.hostkeys
            # And do not verify host key of the new host
            self.cnopts.hostkeys = None
        if self.mesopl_status == None:
            self.check_status()
        yyyy, mm, dd = Utility.create_tr_date_string(tr)
        self.inst_dir = {'NRH': '/radio-solaire/festival/fits/'+yyyy+'/'+yyyy[-2:]+mm.zfill(2),
                         'ORFEE': '/radio-solaire/data/nrhmeudon/rawdata/orfees/'+str(yyyy)+'/'+str(yyyy)[-2:]+str(mm).zfill(2),
                         'WAVES': 0}[inst_name]
        self.filename_exp = {'NRH': (r"^nrh2_1.*_.*_" +
                                     re.escape(yyyy+mm.zfill(2)+dd.zfill(2)) +
                                     r".*.fts$"),
                             'ORFEE': (r"^int_orf.*" +
                                       re.escape(str(yyyy)+str(mm).zfill(2)+str(dd).zfill(2)) +
                                       r".*.fts$")}[inst_name]

    def check_status(self):
        try:
            with pysftp.Connection(host=self.myHostname,
                                   username=self.myUsername,
                                   port=self.thePort,
                                   password=self.myPassword, cnopts=self.cnopts) as sftp:
                if self.hostkeys != None:
                    print("Connected to new host, caching its hostkey")
                    self.hostkeys.add(
                        self.myHostname, sftp.remote_server_key.get_name(), sftp.remote_server_key)
                    self.hostkeys.save(pysftp.helpers.known_hosts())

                print("Mesopl server online")
                self.mesopl_status = True
        except:
            self.mesopl_status = False
            raise ConnectionRefusedError

    def open_connection(self):
        self.sftp = pysftp.Connection(host=self.myHostname,
                                      username=self.myUsername,
                                      port=self.thePort,
                                      password=self.myPassword, cnopts=self.cnopts)
        if self.hostkeys != None:
            print("Connected to new host, caching its hostkey")
            self.hostkeys.add(self.myHostname,
                              self.sftp.remote_server_key.get_name(),
                              self.sftp.remote_server_key)
            self.hostkeys.save(pysftp.helpers.known_hosts())

        print("Connection succesfully stablished ... ")

    def check_file(self, localClient):
        self.open_connection()
        # Switch to a remote directory
        self.sftp.cwd(self.inst_dir)

        # Obtain structure of the remote directory '/var/www/vhosts'
        directory_structure = self.sftp.listdir_attr()

        # Print data
        files_in_dir = [attr.filename for attr in directory_structure]
        for filename in files_in_dir:
            if re.match(self.filename_exp, filename):
                print('Match found')
                self.sftp.get(self.inst_dir + "/" + filename,
                              localClient.OUT_DIR+filename)
            # else:
                #print('File not found')
                # return False
        return True

    def is_datasource_for(self):
        return NotImplemented
