from helioex.ModelClass.DataSourceModel import DataSource
import requests
from cdasws import CdasWs
from datetime import datetime


class CDAWEb(DataSource):

    def __init__(self, tr, inst_name):
        self.tr = tr
        self.instr_dir = {
            'WAVES': 'WI_H1_WAV',
            'WI3DP': 'WI_SFSP_3DP'
        }[inst_name]
        self.var_dir = {
            'WAVES': ['E_VOLTAGE_RAD2', 'E_VOLTAGE_RAD1', 'E_VOLTAGE_TNR'],
            'WI3DP': ['FLUX_STACKED']
        }[inst_name]

    def check_status(self):
        get = requests.get('https://cdaweb.gsfc.nasa.gov/')
        # if the request succeeds
        if get.status_code == 200:
            print('CDAWeb is accessible')
            return True
        else:
            return False

    def open_connection(self):
        self.cdas = CdasWs()

    def check_file(self):
        self.open_connection()
        status, data = self.cdas.get_data(self.instr_dir, self.var_dir,
                                          self.tr.start.datetime.strftime(
                                              '%Y-%m-%dT%H:%M:%SZ'),
                                          self.tr.end.datetime.strftime('%Y-%m-%dT%H:%M:%SZ'))
        if status['http']['status_code'] == 200:
            print('CDAWeb data access successfull.')
            return data
        else:
            print('CDAWeb data not available')
            return False

    def is_datasource_for(self):
        return NotImplemented
