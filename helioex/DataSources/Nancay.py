from helioex.ModelClass.DataSourceModel import DataSource
import requests
from helioex.utils import Utility
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import os
import time


class NancayObs(DataSource):

    def __init__(self, tr):
        self.nancay_status = None

        if self.nancay_status == None:
            self.check_status()

        self.yyyy, self.mm, self.dd = Utility.create_tr_date_string(tr)
        self.OUT_DIR = '/home/tjames/work_stuff/nrh_project/Radio+RHESSI/temp_data'

    def check_status(self):
        r = requests.head('https://realtime.obs-nancay.fr/')
        if (r.status_code == 200):
            print('Nancay server online.')
            self.nancay_status = True

    def check_file(self):
        chromeOptions = webdriver.ChromeOptions()
        prefs = {"download.default_directory": self.OUT_DIR}
        chromeOptions.add_experimental_option("prefs", prefs)
        driver = webdriver.Chrome(
            '/usr/lib/chromium-browser/chromedriver', options=chromeOptions)
        driver.get(
            'https://realtime.obs-nancay.fr/dam/data_dam_affiche/data_dam_affiche.php?init=1&lang=fr')

        try:
            select = Select(driver.find_element_by_class_name('mode'))
            select.select_by_value('fichier')
            select1 = Select(driver.find_element_by_class_name('an'))
            select1.select_by_value(self.yyyy)
            select2 = Select(driver.find_element_by_class_name('mois'))
            select2.select_by_value(self.mm)
            driver.find_element_by_name('mm_cdf').click()
            driver.find_element_by_name('mm_'+self.dd).click()
            driver.find_element_by_class_name('qlM').click()
            driver.find_element_by_partial_link_text(
                'Télécharger le fichier zip').click()
            time.sleep(10)
            driver.close()
            file_path = max([os.path.join(self.OUT_DIR, f)
                            for f in os.listdir(self.OUT_DIR)], key=os.path.getctime)
            return (101, file_path)
        except:
            driver.close()
            return 404

    def open_connection(self):
        pass

    def is_datasource_for(self):
        pass
