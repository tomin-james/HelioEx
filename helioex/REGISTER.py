from helioex.SWITCH import TurnOn
from helioex.Instruments.RHESSI import RHESSI_Meta, RHESSI_Func
from helioex.Instruments.GOES import GOES_Meta, GOES_Func
from helioex.Instruments.FERMI import FERMI_Meta, FERMI_Func
from helioex.Instruments.NRH import NRH_Meta, NRH_Func
from helioex.Instruments.ORFEE import ORFEE_Meta, ORFEE_Func
from helioex.Instruments.NDA import NDA_Meta, NDA_Func
from helioex.Instruments.WAVES import WAVES_Meta, WAVES_Func
from helioex.Instruments.WI3DP import WI3DP_Meta, WI3DP_Func
from helioex.noconflict import makecls


def INSTRUMENTS():
    class RHESSI(RHESSI_Meta, RHESSI_Func, TurnOn):

        @staticmethod
        def status():
            print('im ok,but I died')

    class GOES(GOES_Meta, GOES_Func, TurnOn):

        @staticmethod
        def status():
            print('im ok, and alive')

    class FERMI(FERMI_Meta, FERMI_Func, TurnOn):

        @staticmethod
        def status():
            print('im ok, and alive')

    class NRH(NRH_Meta, NRH_Func, TurnOn):

        @staticmethod
        def status():
            print('im ok and alive')

    class ORFEE(ORFEE_Meta, ORFEE_Func, TurnOn):

        @staticmethod
        def status():
            print('Im ok and alive')

    class NDA(NDA_Meta, NDA_Func, TurnOn):

        @staticmethod
        def status():
            print('Im ok and alive')

    class WAVES(WAVES_Meta, WAVES_Func, TurnOn):

        @staticmethod
        def status():
            print('Im ok and alive')

    class WI3DP(WI3DP_Meta, WI3DP_Func, TurnOn):

        @staticmethod
        def status():
            print('Im ok and alive')
