from abc import ABC, abstractmethod


class DataSource(ABC):

    '''
    Abstract base class for creating datasource pipelines
    to gather instrument data.
    '''

    @abstractmethod
    def check_status(self):
        pass

    @property
    @abstractmethod
    def is_datasource_for(self):
        pass

    @abstractmethod
    def open_connection(self):
        pass

    @abstractmethod
    def check_file(self):
        pass
