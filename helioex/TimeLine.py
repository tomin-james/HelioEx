import matplotlib.pyplot as plt
import matplotlib.dates as plt_dates
import matplotlib.gridspec as gridspec

from helioex.SWITCH import TurnOn
from helioex.utils import Utility


class TimeLine(object):
    '''
    The main class for defining the timeline for an event.
    A PhysicalEvent object can be created by inheriting this class.
    For creating a TimeLine object the user needs to supply
    a time range, in the form of a datetime object, and atleast
    one instrument. More instrumens can be later added or removed.

    '''

    def __init__(self, dt_, **kwargs):
        self.time_range = dt_
        self.config = kwargs['config']
        self._instruments = self.config['instr']
        self.instrument = {}
        self.activated_instrs = {}
        self.data = {}

    @property
    def time_range(self):
        return self._time_range

    @time_range.setter
    def time_range(self, dt_):
        '''

        '''
        self._time_range = Utility.time_range_(dt_[0], dt_[1])

    @staticmethod
    def plot(self):
        pass

    @property
    def instruments(self):
        return self._instruments

    @instruments.setter
    def instruments(self, instr_name: str):
        if instr_name not in self._instruments:
            self._instruments.append(instr_name)

    def instantiate_instruments(self):
        for instr in self._instruments:
            instr_cls = [subclass for subclass in TurnOn.__subclasses__()
                         if (subclass.__name__ == instr.lower()) or
                         (subclass.__name__ == instr.upper())][0]
            if instr_cls not in self.instrument.keys():
                self.instrument[instr_cls.__name__] = instr_cls
        for instr_cls in self.instrument.keys():
            if instr_cls not in self.activated_instrs.keys():
                self.activated_instrs[instr_cls] = self.instrument[instr_cls]()

    def get_data(self):
        for instr_client in self.activated_instrs.keys():
            self.data[instr_client] = self.activated_instrs[instr_client].fetch_data(
                self.time_range, config=self.config)

    def plot_data(self, instr, align_time=False):
        if len(instr) == 1:
            fig = plt.figure(figsize=(9, 3), facecolor='w')
            axes = fig.subplots(nrows=1, ncols=1)
            self.activated_instrs[instr[0].upper()].plot_data(
                self.data[instr[0].upper()], axes)
            date_format = plt_dates.DateFormatter('%H:%M')
            axes.xaxis.set_major_formatter(date_format)
            axes.text(0.5, -0.2, self.time_range.start.strftime("%d/%m/%Y"), ha='center', va='center',
                      transform=axes.transAxes)
            axes.set_title(
                f"Instrument: {instr[0].upper()} data - {self.activated_instrs[instr[0].upper()]._quant_measured}.")

        elif len(instr) > 1:
            fig = self.plot_factory(instr, align_time)

    def plot_factory(self, instr, align_time):
        fig = plt.figure(constrained_layout=True,
                         figsize=(len(instr)*1.5, len(instr)*3))

        if not align_time:
            G = gridspec.GridSpec(
                len(instr), 1, figure=fig, hspace=0.5, wspace=0)
            for plot_id, instr_name in enumerate(instr):
                axes_ = plt.subplot(G[plot_id, :])
                self.activated_instrs[instr_name.upper()].plot_data(
                    self.data[instr_name.upper()], axes_)

        if align_time:
            fig, axes_ = plt.subplots(nrows=len(instr), ncols=1, figsize=(len(instr)*1.5, len(instr)*3), facecolor='w', sharex=True,
                                      sharey=False, gridspec_kw={'hspace': 0, 'wspace': 0.07})
            # subfigs = fig.subfigures(1, 1, wspace=0.07)
            # axes_ = subfigs.subplots(len(instr), 1, sharex=True)
            for plot_id, instr_name in enumerate(instr):
                self.activated_instrs[instr_name.upper()].plot_data(
                    self.data[instr_name.upper()], axes_[plot_id])

        fig.text(0.5, 0.09, self.time_range.start.strftime(
            "%d/%m/%Y"), ha='center', va='center')
        return fig
