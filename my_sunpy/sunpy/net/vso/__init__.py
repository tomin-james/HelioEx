# for exposure to from sunpy.net.vso import *
from my_sunpy.sunpy.net.vso.vso import QueryResponse, VSOClient

__all__ = ['VSOClient', 'QueryResponse']
