
# Import and register the clients but we do not want them in the namespace, we import them as _
from my_sunpy.sunpy.net import base_client as _
from my_sunpy.sunpy.net import dataretriever as _
from my_sunpy.sunpy.net import jsoc as _
from my_sunpy.sunpy.net import vso as _
from my_sunpy.sunpy.net.fido_factory import Fido

__all__ = ["Fido"]
