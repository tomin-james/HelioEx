from my_sunpy.sunpy.net.attr import SimpleAttr

__all__ = ['SatelliteNumber']


class SatelliteNumber(SimpleAttr):
    """
    The GOES Satellite Number
    """
