"""
This module provies a collection of datasource-specific
`~sunpy.timeseries.TimeSeries` classes.

Each mission should have its own file with one or more classes defined.
Typically, these classes will be subclasses of the
`sunpy.timeseries.TimeSeries`.
"""
from my_sunpy.sunpy.timeseries.sources.eve import ESPTimeSeries, EVESpWxTimeSeries
from my_sunpy.sunpy.timeseries.sources.fermi_gbm import GBMSummaryTimeSeries
from my_sunpy.sunpy.timeseries.sources.goes import XRSTimeSeries
from my_sunpy.sunpy.timeseries.sources.lyra import LYRATimeSeries
from my_sunpy.sunpy.timeseries.sources.noaa import NOAAIndicesTimeSeries, NOAAPredictIndicesTimeSeries
from my_sunpy.sunpy.timeseries.sources.norh import NoRHTimeSeries
from my_sunpy.sunpy.timeseries.sources.rhessi import RHESSISummaryTimeSeries
