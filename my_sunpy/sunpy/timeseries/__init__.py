from my_sunpy.sunpy.timeseries.metadata import TimeSeriesMetaData
from my_sunpy.sunpy.timeseries.sources import *
from my_sunpy.sunpy.timeseries.timeseries_factory import TimeSeries
from my_sunpy.sunpy.timeseries.timeseriesbase import GenericTimeSeries

try:
    # Register pandas datetime converter with matplotlib
    import pandas.plotting
    pandas.plotting.register_matplotlib_converters()
except ImportError:
    pass

__all__ = ["TimeSeriesMetaData", "TimeSeries", "GenericTimeSeries"]
