# HelioEX

A data pipeline to handle solar physics data originating from multiple space and ground based instruments.
This is intendend as a one-stop solution to study and predict space-weather with advanced ML tools built-in.

Solar physics is a data rich field. There are many observational facilities both ground based and space based.
Often multi-wavelength studies are needed to understand the rich physics influencing various phenomenons in Heliophysics.
For example, to study electron acceleration in flare it is important to look at both the simultaneous emissions 
in the radio and x-ray domain. However, finding such events using multi-wavelength data is often a vary laborius
job. This software was developed as platform for automatic surveys of multi-wavelength data in heliophysics.

# Description

There are many data repositiories for solar physics. This has often resulted in data being confiined in silos.
Researchers often have to work very hard to combine data from different sources. The main agenda of this software,
is to act as a onestop solution for combining and searcing for mulit-variety data.
Initially, this software was developed for exclusive use within the Paris Observatory network. 
Thus many functionalities implemented here relies heavily on the data which is accessible only within the Paris observatory network.

The software is written in a very modular way. The aim is to reduce the complexity of adding a new instrument to be minimal.
There are base classes which standardises the implementation for any instrument.

Currently the software can handle, query and do integrated search for data from the following instruments.
- RHESSI
- FERMI
- Nancay Radio Heliograph (NRH)
- ORFEES
- CALLISTO
- Nancay Decametric Array (NDA)
- WIND/WAVES

In the current configuration the software can track particles accelerated in a solar flare site ( detected using HXR radiation) 
and their journey through the interplanetary space. The user can specify the necessary conditions to meet - like time difference, intensity,
class of flare etc to select particular classes of events.


![alt text](imgs/1.png "Timeline of an event")

The above figure shows the timeline of an event. The bottom two panels show the HXR bursts, while the panels above that shows radio emissions.

![alt text](imgs/2.png "Lightcurve timeline of the event")

Tha above figure shows the detection of radio and HXR flares using lightcurves. This functionality has limited utility now as the NRH lightcurves,
needs to be manually imported. In the future, the idea is to develop methods where such light curves could be directly calculated from within the 
imaging fits files.


## Installation
The module relies on the access to the data repository to the Paris Observatory server. 
Hence user credentials needs to be supplied for sftp file transfers needed for the software to work.

## Roadmap
Currently, we are planning to extend it other instruments in wavelengths covering EUV,AIA filter bands and magnetograms.


## Contributing
I would love if you could contribute more to this project.
You can contact me more details via [email](mailto:tomin.james@outlook.com?subject=Meenkando-contribution)

## Authors and acknowledgment
The Paris Observatory team maintaining this project has been especially supportive of this work.
I specifically thanks Abdallah Hamini for the help.


## License
The project is licensed under MIT.

## Project status
The project is being acively developed. So do contact if you have more ideas.

